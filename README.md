# Views remote pager (VRP)

## Motivation
Whenever it comes to extending the possibilites of your book, term or any other
ordering or filtering system for nodes, you will end up creating a view to list
an arbitrarily set of node teasers.

However, once you click on one of those teaser links, the related node will be
displayed on its node/$nid display, and all context to e. g. the previous
or next node from the said views result is gone.

In some very simple cases, creating a "book" may help. But as soon as the node
is tagged with more than one term, there is already no more reliable way to
reproduce the order a reader would expect.

A common practice is to create an additional display for that view, probably
with a one-item paging and a full node display. This, again, comes at the price
of sacrificing the advantages of a dedicated full node display, such as
path aliasing, node-specific block placement and probably more.

## Approach
The module tries to get the best of all these worlds. Whenever a node is rendered,
it tries to recognize whether the node is part of a views result set. If so,
the $node_url variable passed to the node template is extended by a query holding
information on the originating view.

Then, whenever a node page is requested through such a link, the HTTP query
is examined on containing such view informations, and if it does, a pager which
reflects the current node's position in the corresponding view's result is being
calculated and can be injected in various ways (block, display widget, headers
in the HTML document ("rel" links in the header scope).

This way, whenever your visitors request an arbitrarily views-ordered node list,
they can navigate through the results without them ever jumping back to that
result page, and without you needing to create any duplicate and probably
inconsistent duplicate structures (books, term menus etc).

(tbd)
