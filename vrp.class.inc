<?php

/**
 * Internal toolset for Views remote pager module.
 */

class VRP {

  // Internal caching.
  protected static $navigation, $pager;

  // Holds a registry of views displays related to node IDs
  protected static $nodeViews;

  // Related to original view.
  protected static $view, $nid, $position;

  /**
   * Adds a vrp query to the url of any node which is result of a VRP view.
   *
   * @param object $node
   *   A node object about to be linked.
   *
   * @return bool|array
   *   If VRP data was found in the query,
   */
  public static function getViewParams($node = NULL) {

    $ret = FALSE;
    if (
      ($view = self::getView($node))
      &&
      // Check if the display is vrp-enabled.
      (self::getVRPOption($view, 'vrp_enabled'))
      &&
      // Check if this actually is a paged result.
      (isset($view->row_index))
      &&
      // Make sure a views ID is provided.
      ($source_view_id =@ $view->name)
      &&
      // Make sure a views ID is provided.
      ($display = self::getActiveDisplay($view))
    ) {
      // Prepare query parameters.
      $original_query = $_GET;
      unset($original_query['q']);
      $params = array(
        'v' => $source_view_id,
        'd' => $display->id,
      );
      if ($original_query = drupal_http_build_query($original_query)) {
        $params['q'] = $original_query;
      }
      // Add path parameter for return in case the parent views display
      // does NOT provide a "path" property:
      if (!isset($display->display_options) || !isset($display->display_options['path'])) {
        $params['p'] = current_path();
      }
      // Track arguments.
      if ($arguments =@ $view->args) {
        $params['a'] = $arguments;
      }
      // Add views internal params. These may not live in the query,
      // but reflect the default settings for exposed filters.
      if (($views_query =@ $view->exposed_raw_input) && is_array($views_query)) {
        $params['vq'] = urlencode(drupal_http_build_query($views_query));
      }

      $ret = array('vrp' => $params);
    }
    return $ret;
  }

  /**
   * Builds a navigation from the current global request, if it indicates so.
   *
   * Checks the request data for existing views data. If such data is found,
   * the corresponding view is looked up, executed and the position of the
   * current node in the result set is evaluated.
   *
   * @param bool $force
   *   Rebuild, even if previously cached.
   *
   * @return array|bool
   *
   */
  public static function getNavigationData($force = FALSE) {
    if (!isset(self::$navigation) || $force) {

      $ret = FALSE; // Default.

      // Remove original path request and remember original query.
      $originalQuery = $_GET;
      unset($originalQuery['q']);

      // Will currently only work on node pages.
      if ($node = self::getNode()) {
        if ($nid =@ $node->nid) {
          // Evaluate VRP query data, build and execute view.
          if ($query =@ $originalQuery['vrp']) {
            if (($view_id =@ $query['v']) && ($display_id =@ $query['d'])){
              if ($view =@ views_get_view($view_id)) {
                if ($view->set_display($display_id)) {
                  // Make views display accessible.
                  $currentDisplay = $view->display[$display_id];
                  // Remember original pager limit.
                  $originalLimit = 0;
                  if ($originalPager =@ $currentDisplay->display_options['pager']) {
                    $originalLimit = (int) $originalPager['options']['items_per_page'];
                  }
                  if ($args =@ $query['a']) {
                    $view->set_arguments($args);
                  }
                  if ($exposed =@ $query['vq']) {
                    if ($exposed = drupal_get_query_array(urldecode($exposed))) {
                      $view->set_exposed_input($exposed);
                    }
                  }
                  // Alter view parameters to get all items.
                  $view->set_items_per_page(0);
                  $view->set_offset(0);
                  // Execute updated view to reconstruct actual paging data.
                  $view->build();
                  $view->pre_execute();
                  $view->execute();
                  if (!empty($view->result)) {
                    // Identify node position in the view.
                    foreach ($view->result as $pos => $data) {
                      if ($data->nid == $nid) {
                        // Will ignore non-distinct queries by design, so multiple positions
                        // will not work.
                        $currentPosition = $pos;
                        break;
                      }
                    }
                    $currentPage = $originalLimit > 0 ? floor($currentPosition / $originalLimit) : 0;
                    $max = count($view->result) - 1;

                    // Prepare result array:
                    $ret = array();
                    if ((!$options =@ $currentDisplay->display_options) || (!$parentPath =@ $options['path'])) {
                      $parentPath =@ $query['p'];
                    }
                    else {
                      // Remove argument placeholders.
                      $parentPath = str_replace('/%', '', $parentPath);
                    }
                    if (!empty($parentPath)) {
                      if ($arguments =@ $query['a']) {
                        array_unshift($arguments, $parentPath);
                        $parentPath = implode('/', $arguments);
                      }
                      $ret['up'] = $parentPath;
                    }
                    if ($currentPosition > 0) {
                      $ret += array(
                        'first' => 0,
                        'previous' => $currentPosition - 1,
                      );
                    }

                    if ($currentPosition < $max) {
                      $ret += array(
                        'next' => $currentPosition + 1,
                        'last' => $max,
                      );
                    }

                    // Build navigation links; respect original query.
                    foreach (array_keys($ret) as $key) {
                      // Create actual path except for "up" (already done):
                      $newQuery = $originalQuery;
                      if ($key == 'up') {
                        $path = $ret[$key];
                        if ($currentPage) {
                          $newQuery['page'] = $currentPage;
                        }
                        if (!empty($exposed)) {
                          $newQuery += $exposed;
                        }
                        unset($newQuery['vrp']); // Not needed in the parent view!
                        $fragment = 'node-' . $nid;
                        $targetNid = NULL;
                      }
                      else {
                        // Determine node nid from views result
                        // (is always present in "content" row style).
                        $pos = $ret[$key];
                        $targetNid = $view->result[$pos]->nid;
                        $path = "node/$targetNid";
                        $fragment = NULL;
                      }
                      $ret[$key] = array(
                        'href' => url($path, array(
                          'query' => $newQuery,
                          'fragment' => $fragment,
                        )),
                        'path' => $path,
                        'query' => $newQuery,
                        'fragment' => $fragment,
                        'targetNid' => $targetNid,
                      );
                    };
                    // Cache the view for for later references.
                    self::$view = $view;
                  }
                }
              }
            }
          }
        }
      }
      self::$navigation = $ret;
    }
    return self::$navigation;
  }

  /**
   * Sets the breadcrumb, if configured.
   */
  public static function setBreadcrumb() {
    if (
      ($view =@ self::$view)
      &&
      (self::getVRPoption($view, 'vrp_breadcrumb'))
      &&
      ($link = self::getParentLink())
    ) {
      drupal_set_breadcrumb(array($link));
    }
  }

  /**
   * Adds "<link rel=...>" tags depending on the actual navigation state.
   */
  public static function injectRelations() {
    if ($nav = self::getNavigationData()) {
      foreach ($nav as $type => $data) {
        drupal_add_html_head(array(
          '#tag' => 'link',
          '#attributes' => array( // Set up an array of attributes inside the tag
            'rel' => $type,
            'href' => $data['href'],
          ),
        ), "vrp_nav_$type");
      }
    }
  }

  /**
   * Builds a render array similar to core pagers.
   *
   * @return array
   *   A pager render array (may be empty if there is nothing to page).
   */
  public static function getPager() {
    if (!isset(self::$pager)) {
      $ret = array();
      // Initialize internal views data and check if there is something to page.
      if ($navData = self::getNavigationData()) {
        $pagerDefaults = array(
          'first' => t('First'),
          'previous' => t('Previous'),
          'up' => t('Up'),
          'next' => t('Next'),
          'last' => t('Last'),
        );
        $listItems = array();
        $useLabels = self::getVRPoption(self::$view, 'vrp_labels');
        foreach ($pagerDefaults as $key => $title) {
          // Resolve link titles, if configured.
          if ($useLabels) {
            if ($key == 'up') {
              $title = self::getParentTitle($title);
            }
            elseif ($targetNid =@ $navData[$key]['targetNid']) {
              $title = self::getNodeTitle($targetNid, $title);
            }
          }
          $label = '<span class="icon"></span><span class="text">' . $title . '</span>';
          if (!empty($navData[$key])) {
            // Add a link.
            $element = l($label, $navData[$key]['path'], array(
              'query' => $navData[$key]['query'],
              'fragment' => $navData[$key]['fragment'],
              'html' => TRUE,
              'attributes' => array(
                'title' => $title,
              ),
            ));
          }
          else {
            // Add plain text.
            $element = "<span class=\"inactive\">$label</span>";
          }
          $listItems[] = array(
            'data' => $element,
            'class' => array("pager-$key"),
          );
        }
        $ret = array(
          '#theme' => 'item_list',
          '#items' => $listItems,
          '#attributes' => array(
            'class' => array('vrp-pager', 'pager'),
          ),
        );
      }
      self::$pager = $ret;
    }
    return self::$pager;
  }

  /**
   * Renders a link to the parent view.
   *
   * @return string|FALSE
   */
  protected static function getParentLink() {
    if (
      ($nav = self::getNavigationData())
      &&
      ($title = self::getParentTitle())
    ) {
      if ($up =@ $nav['up']) {
        return l($title, $up['path'], array(
          'query' => $up['query'],
          'fragment' => $up['fragment'],
          'html' => TRUE,
        ));
      }
    }
    return FALSE;
  }

  /**
   * Returns the escaped public title for the parent view.
   *
   * @param string $default
   *   (Optional) fallback to use if no title is available.
   *
   * @return string|FALSE
   */
  protected static function getParentTitle($default = FALSE) {
    if ($view =@ self::$view) {
      if ($title = $view->get_title()) {
        return $title;
      }
    }
    return $default;
  }

  /**
   * Determines a node from the current page (path).
   *
   * @return object|FALSE
   */
  protected static function getNode() {
    // Try to determine node ID if not given.
    foreach (range(1, 3) as $i) {
      if ($node = menu_get_object('node', $i)) {
        return $node;
      }
    }
    if (!isset($node) && arg(0) == 'node') {
      return node_load(arg(1));
    }
    return FALSE;
  }

  /**
   * Returns the active view for a given node.
   *
   * @param int|object $node
   *   (Optional) A node object or an nid to load the node from.
   *   If no value is provided, the currently displayed node page
   *   is assumed.
   *
   * @return object|bool
   *   The corresponding views object or FALSE on errors.
   */
  protected static function getView($node = NULL) {

    // Try to determine node ID if not given.
    if (!isset($node)) {
      $node = self::getNode();
    }

    if (!empty($node)) {

      // Determine nid.
      if ($nid = (is_object($node) ? $node->nid : (int) $node)) {

        // Init static cache.
        if (!isset(self::$nodeViews)) {
          self::$nodeViews = array();
        }

        // Look up actual node/view relation.
        if (!isset(self::$nodeViews[$nid])) {

          // Set default.
          self::$nodeViews[$nid] = FALSE;

          if (!is_object($node)) {
            $node = node_load($node);
          }
          if (($view =@ $node->view) && (is_object($view))) {
            self::$nodeViews[$nid] = $view;
          }
        }
        return self::$nodeViews[$nid];
      }
    }
    return FALSE;
  }

  /**
   * Returns the active display object of a given view.
   *
   * @param object $view
   *
   * @return object|bool
   */
  protected static function getActiveDisplay($view) {
    if (
      (is_object($view))
      &&
      ($display =@ $view->display[$view->current_display])
    ) {
      return $display;
    }
    return FALSE;
  }

  /**
   * Returns VRP options for the active display of a given view.
   *
   * @param object $view
   * @param string $key
   *
   * @return mixed $value
   */
  protected static function getVRPOption($view, $key) {
    if (
      ($display = self::getActiveDisplay($view))
      &&
      ($options =@ $display->display_options['vrp'])
      &&
      (is_array($options))
    ) {
      return (bool) @$options[$key];
    }
  }

  /**
   * Retrieve a node title.
   *
   * @param int|object $node
   *   Node or NID.
   * @param string $default
   *   (Optional) fallback title.
   *
   * @return string|bool
   *   Corresponding node title, or default (if provided), or FALSE.
   */
  protected static function getNodeTitle($node, $default = FALSE) {
    if (!is_object($node)) {
      $node = node_load($node);
    }
    if (!empty($node)) {
      return $node->title;
    }
    return $default;
  }

}
