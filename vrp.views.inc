<?php

/**
 * @file
 * Adds Views3 support.
 *
 * Views3 adds the concept of exposed forms to the mix.  In addition, elements
 * injected into a Views dialog is no longer saved along with the Views form
 * information (see the unpack_options() and options_definition() methods of the
 * views_object object).
 */

/**
 * Implements hook_views_plugins().
 */
function vrp_views_plugins() {
  return array(
    'display_extender' => array(
      'vrp' => array(
        'title' => t('Remote pager'),
        'help' => t('Adds navigation between views result nodes.'),
        'handler' => 'vrp_plugin',
      ),
    ),
  );
}
