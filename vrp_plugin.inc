<?php

/**
 * @file
 * Allows to enable Views remote pager functionality for views displays.
 */

class vrp_plugin extends views_plugin_display_extender {

  function option_definition() {
    $options = parent::option_definition();
    $options['vrp'] = array(
      'vrp_enabled' => array(
        'default' => TRUE,
        'bool' => TRUE,
      ),
      'vrp_labels' => array(
        'default' => TRUE,
        'bool' => TRUE,
      ),
      'vrp_breadcrumb' => array(
        'default' => TRUE,
        'bool' => TRUE,
      ),
    );
    return $options;
  }

  /**
   * Default values.
   */
  function options_definition_alter(&$options) {
    $options['vrp'] = array(
      'default' => array(
        'vrp_enabled' => TRUE,
        'vrp_labels' => FALSE,
        'vrp_breadcrumb' => TRUE,
      ),
    );
    return $options;
  }

  /**
   * Defines where within the Views admin UI the new settings will be visible.
   */
  function options_summary(&$categories, &$options) {
    $opts = $this->display->get_option('vrp');
    $yes = $opts['vrp_labels'] ? t('Yes (titles as labels)') : t('Yes');
    $is_enabled = $opts['vrp_enabled'] ? $yes : t('No');
    $options['vrp'] = array(
      'category' => 'pager',
      'title' => t('Enable remote pager'),
      'value' => $is_enabled,
    );
  }

  function options_form(&$form, &$form_state) {
    $opts = $this->display->get_option('vrp');
    if ($form_state['section'] == 'vrp') {
      parent::options_form($form, $form_state);
      $form['#title'] .= t('Remote pager options');
      $form['vrp_enabled'] = array(
        '#type' => 'checkbox',
        '#title' => t('Enable remote pager'),
        '#default_value' => $opts['vrp_enabled'],
        '#description' => t('Whether to provide virtual paging for nodes linked by this view.'),
      );
      $form['vrp_labels'] = array(
        '#type' => 'checkbox',
        '#title' => t('Titles as labels'),
        '#default_value' => $opts['vrp_labels'],
        '#description' => t('Whether to display node/view titles instead of "next"/"last"/"up".'),
      );
      $form['vrp_breadcrumb'] = array(
        '#type' => 'checkbox',
        '#title' => t('Set breadcrumb'),
        '#default_value' => $opts['vrp_breadcrumb'],
        '#description' => t('If enabled, the breadcrumb of a paged node is set to the parent view.'),
      );
    }
  }

  /**
   * Saves the form values.
   */
  function options_submit(&$form, &$form_state) {
    $v = $form_state['values'];
    $opts = $this->display->get_option('vrp');
    $opts['vrp_enabled'] = $v['vrp_enabled'];
    $opts['vrp_labels'] = $v['vrp_labels'];
    $opts['vrp_breadcrumb'] = $v['vrp_breadcrumb'];
    $this->display->set_option('vrp', $opts);
  }
}
